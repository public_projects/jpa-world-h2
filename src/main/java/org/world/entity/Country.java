package org.world.entity;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Country implements java.io.Serializable {

    @Id
    @Column(name="country_id")
    private Integer country_id;
    
    @OneToMany(cascade={CascadeType.ALL}, mappedBy = "country")
    private Set<Citizen> citizen;
    
    @Column
    String name;
    
    @Column
    String currency;

    public Country(){}

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCountry_id() {
        return country_id;
    }

    public void setCountry_id(Integer country_id) {
        this.country_id = country_id;
    }

    public Set<Citizen> getCitizen() {
        return citizen;
    }

    public void setCitizen(Set<Citizen> citizen) {
        this.citizen = citizen;
    }
}
